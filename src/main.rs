#![feature(underscore_lifetimes)]
extern crate csv;
extern crate serde;
#[macro_use]
extern crate enum_map;
#[macro_use]
extern crate lazy_static;

pub mod kbo;
use kbo::kbo_reader::{KboError, KboReader};
use kbo::kbo_file::KboFile::*;

fn main() {
    match filter_kbo("./tmp/kbo") {
        Ok(_) => (),
        Err(_) => println!("Waar zitten de csv's ?"),
    }
}

fn filter_kbo(kbo_dir: &str) -> Result<(), KboError> {
    let rdr = KboReader::new(kbo_dir);
    let forms = vec!["017", "028", "117", "125"];
    let postcodes = vec!["1082"];

    let entr_set = rdr.filter_ids(Enterprise, &forms)?;
    println!("Aantal VZW's:                {:?}", entr_set.len());
    let addr_set = rdr.filter_ids(Address, &postcodes)?;
    println!("Aantal ondernemingen in SAB: {:?}", addr_set.len());
    let inte_set: std::collections::HashSet<_> = entr_set.intersection(&addr_set).collect();
    println!("Aantal VZW's in SAB:         {:?}", inte_set.len());
    println!("Voorbeeld:\n{:?}", inte_set.iter().last());

    Ok(())
}
