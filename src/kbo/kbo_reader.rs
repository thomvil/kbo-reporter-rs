use csv;
use super::csv_reader::CsvReader;
use super::record::*;
use super::kbo_file::*;

pub type KboError = csv::Error;

pub struct KboReader<'a> {
    kbo_dir: &'a str,
}

impl<'a> KboReader<'a> {
    pub fn new(kbo_dir: &str) -> KboReader {
        KboReader { kbo_dir }
    }

    pub fn fetch_records(&self, kbo_file: KboFile, values: &[&str]) -> RecordListResult {
        let kbo = kbo_spec(kbo_file);
        let filter = Some((kbo.filter_id, values));
        CsvReader::new(self.kbo_dir, kbo.fname).fetch_records(filter)
    }

    pub fn filter_ids(&self, kbo_file: KboFile, values: &[&str]) -> RecordIdsResult {
        let kbo = kbo_spec(kbo_file);
        let filter = (kbo.filter_id, values);
        CsvReader::new(self.kbo_dir, kbo.fname).filter_ids(filter, kbo.kbo_id)
    }
}
