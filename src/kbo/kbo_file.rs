use enum_map::EnumMap;
use super::record::Record;

#[derive(Clone, Debug, EnumMap)]
pub enum KboFile {
    Activity,
    Address,
    Denomination,
    Enterprise,
    Establishment,
    Contact,
}

#[derive(Clone, Debug)]
pub struct KboFileSpec<'a> {
    pub fname: &'a str,
    pub kbo_id: &'a str,
    pub filter_id: &'a str,
}

impl<'a> KboFileSpec<'a> {
    fn new(fname: &'a str, kbo_id: &'a str, filter_id: &'a str) -> KboFileSpec<'a> {
        KboFileSpec {
            fname,
            kbo_id,
            filter_id,
        }
    }
}

lazy_static! {
    pub static ref KBO_DEFAULTS: EnumMap<KboFile, KboFileSpec<'static>> = enum_map! {
        KboFile::Activity => KboFileSpec::new("activity.csv", "EntityNumber", ""),
        KboFile::Address => KboFileSpec::new("address.csv", "EntityNumber", "Zipcode"),
        KboFile::Denomination => KboFileSpec::new("denomination.csv", "EntityNumber", ""),
        KboFile::Enterprise => KboFileSpec::new("enterprise.csv", "EnterpriseNumber", "JuridicalForm"),
        KboFile::Establishment => KboFileSpec::new("establishment.csv", "EstablishmentNumber", ""),
        KboFile::Contact => KboFileSpec::new("contact.csv", "EntityNumber", ""),
    };
}

pub fn kbo_spec(kbo_file: KboFile) -> KboFileSpec<'static> {
    KBO_DEFAULTS[kbo_file].clone()
}

pub fn contact(mut rec: Record) -> Option<String> {
    match rec.remove("ContactType") {
        Some(res) => Some(res + ": " + &rec.remove("Value").unwrap()),
        None => None,
    }
}
