use csv;
use csv::{Error, Reader};
use std::fs::File;
use std::path::{Path, PathBuf};

use super::record::*;

pub type ReaderResult = Result<Reader<File>, Error>;

pub struct CsvReader {
    csv_path: PathBuf,
}

impl CsvReader {
    pub fn new(csv_dir: &str, csv_file: &str) -> CsvReader {
        let csv_path = Path::new(csv_dir).join(csv_file);
        CsvReader { csv_path }
    }

    pub fn fetch_records(&self, filter: Option<RecordFilter>) -> RecordListResult {
        let list = self.rdr()?
            .deserialize()
            .filter_map(|rec: RecordResult| rec.ok())
            .filter(|rec| rec.is_match(filter))
            .collect();
        Ok(list)
    }

    pub fn filter_ids(&self, filter: RecordFilter, map_key: &str) -> RecordIdsResult {
        let (filter_key, filter_values) = filter;
        let set = self.rdr()?
            .into_deserialize()
            .filter_map(|rec: RecordResult| rec.ok())
            .filter(|rec| rec.is_match_raw(filter_key, filter_values))
            .filter_map(|mut rec| rec.remove(map_key))
            .collect();
        Ok(set)
    }

    fn rdr(&self) -> ReaderResult {
        csv::Reader::from_path(&self.csv_path)
    }
}
