use std::collections::{HashMap, HashSet};
use csv::Error;

pub type Record = HashMap<String, String>;
pub type RecordResult = Result<Record, Error>;
pub type RecordList = Vec<Record>;
pub type RecordListResult = Result<RecordList, Error>;
pub type RecordIds = HashSet<String>;
pub type RecordIdsResult = Result<RecordIds, Error>;
pub type RecordFilter<'a> = (&'a str, &'a [&'a str]);

pub trait RecordMatch {
    fn is_match_raw(&self, key: &str, values: &[&str]) -> bool;
    fn is_match(&self, filter: Option<RecordFilter>) -> bool;
    fn match_all(&self, filters: &[RecordFilter]) -> bool;
}

impl RecordMatch for Record {
    fn is_match_raw(&self, key: &str, values: &[&str]) -> bool {
        values.iter().any(|value| {
            self.get(key)
                .map(|rec_val| rec_val == value)
                .unwrap_or(false)
        })
    }

    fn is_match(&self, filter: Option<RecordFilter>) -> bool {
        filter
            .map(|(key, values)| self.is_match_raw(key, values))
            .unwrap_or(true)
    }

    fn match_all(&self, filters: &[RecordFilter]) -> bool {
        filters.iter().all(|filter| self.is_match(Some(*filter)))
    }
}
